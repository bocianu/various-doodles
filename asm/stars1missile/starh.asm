    icl 'atari.hea'

starX   = $3000  

    org $2000  

init
    ldx #255     ; generate random stars h-positions array
@
    lda random      
    sta starX,x
    dex
    bne @-

    lda #14      ; set color and PMG missile0 bit
    sta 704
    sta grafm   

loop
    lda vcount   ; wait for vcount = 0
    bne loop
@   
    sta wsync    ; wait for new line
    lda starX,x  ; set missile0 horizontal position
    sta hposm0   ; from array
    inc starX,x  ; and move it right 1 px
    dex 
    bne @-        
    
    beq loop     

    run init
