    icl 'atari.hea'

starX   = $3000
ofs     = $fe
dirId   = $fd
dir     = $fc
dirCnt  = $fb

    org $2000  

init
    ldx #255
    stx dirCnt
@
    lda random
    sta starX,x
    dex
    bne @-
   
    lda #14
    sta 704
    sta grafm
    ldx #0 
    stx dirId
    lda dirs,x
    sta dir

loop
    lda vcount
    bne loop
    ldx ofs
    ldy #255
@   
    sta wsync
    lda starX,x
    sta hposm0
    lda dir 
    and #%0010
    beq @+
    inc starX,x // right
@
    lda dir 
    and #%1000
    beq @+
    dec starX,x // left
@
    dex          
    dey
    bne @-2 

    lda dir 
    and #%0100
    beq @+
    inc ofs     // down
@

    lda dir 
    and #%0001
    beq @+
    dec ofs     // up
@

    dec dirCnt
    lda dirCnt
    and #%11111
    bne loop
    inc dirId
    ldx dirId
    cpx #8
    bne @+
    ldx #0
    stx dirId
@    
    lda dirs,x
    sta dir

    jmp loop     

dirs
    dta %0001
    dta %0011
    dta %0010
    dta %0110
    dta %0100
    dta %1100
    dta %1000
    dta %1001

    run init
