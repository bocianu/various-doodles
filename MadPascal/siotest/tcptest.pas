Program tcptest;

Uses atari, crt, fn_sio, sio;

Const 
    R_BUFF_SIZE =   256;

Var FNuri:   PChar =   'N:TCP://fujinet.pl:45666/'#0;
    status:   FN_StatusStruct ;
    bytesWaiting: word = 0;
    bufferDataSize: word = 0;
    dataFlag:   byte = 0;
    oldHandler:   word;
    Buffer: array [0..R_BUFF_SIZE-1] Of byte absolute $4000;

    // ************************************************** NETWORK ROUTINES

Procedure DataFlagHandler:interrupt;assembler;
asm {
    pla
    lda #$01
    sta dataFlag
};
End;

procedure ReadStatusFromSio;
begin
    FN_ReadStatus(status);
    bytesWaiting := status.dataSize;  
    dataFlag := 0;
end;

// ************************************************** 
// ************************************************** MAIN PROGRAM
// ************************************************** 

Begin
    PACTL := PACTL Or 1;
    // set Interrupt
    asm { sei};
    oldHandler := VPRCED;
    VPRCED := word(@DataFlagHandler);
    asm { cli};

    // connect
    Write('Connecting: ');
    Writeln(FNuri);
    FN_Open(FNuri);

    lmargin := 0;
    ClrScr;

    // main communication loop
    Repeat
        if (dataFlag = 1) then begin 
            ReadStatusFromSio;
            Write('Waiting:');Writeln(bytesWaiting);
        end;
        If bytesWaiting > 0 Then begin
            bufferDataSize := bytesWaiting;
            if bufferDataSize > R_BUFF_SIZE then bufferDataSize := R_BUFF_SIZE;
            FN_ReadBuffer(Buffer, bufferDataSize);
            Write('Reading:');Writeln(bufferDataSize);
            bytesWaiting := bytesWaiting - bufferDataSize;
        end;
        pause(10);
    Until false;

    Writeln('Closing connection...');
    FN_Close;
    asm { sei};
    VPRCED := oldHandler;
    asm { cli};
End.
