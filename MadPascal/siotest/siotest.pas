program siotest;
uses crt, fn_sio, sio;

var hostname: TString = 'N:TCP://fujinet.pl:80/';
    req: TString = 'GET / HTTP/1.0'#13#10#13#10;

var
    recv_buffer: array [0..1024] of byte;
    status: array [0..3] of byte;
    i,bytesWaiting: word;

begin
    Write('Connecting to: '); Writeln(hostname);
    FN_Open(hostname);
    Writeln(sioResult);
    if sioResult = 1 then begin
        
        FN_WriteBuffer(@req[1],byte(req[0]));
        Writeln(sioResult);
        
        repeat
            Pause(10);
            FN_ReadStatus(status);
            bytesWaiting := status[1] * 256 + status[0];
            if bytesWaiting > 0 then begin
                FN_ReadBuffer(@recv_buffer, bytesWaiting);
                for i:=0 to bytesWaiting-1 do Write(char(recv_buffer[i]));
            end;
        until keypressed;

    end;

    Write('Closing connection: ');
    FN_Close;
    Writeln(sioResult);
end.
