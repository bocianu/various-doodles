unit fn_sio;
interface
uses sio;

type FN_StatusStruct = Record
    dataSize: word;
    connected: byte;  
    errorCode: byte;
end;

procedure FN_Open(var fn_uri:PChar);

procedure FN_WriteBuffer(buf: pointer;len: word);

procedure FN_ReadBuffer(buf: pointer;len: word);

procedure FN_ReadStatus(status: pointer);

procedure FN_Close;


implementation

procedure FN_Open(var fn_uri:PChar);
begin
    PACTL := PACTL or 1;
    DCB.DDEVIC := $71;
    DCB.dunit := 1;
    DCB.DCMND := byte('O');
    DCB.DSTATS := _W;
    DCB.DBUFA := word(@fn_uri);
    DCB.DTIMLO := $0f;
    DCB.DBYT := 256;
    DCB.DAUX1 := 12;    
    DCB.DAUX2 := 0;   
    ExecSIO;
end;

procedure FN_WriteBuffer(buf: pointer;len: word);
begin
    DCB.DDEVIC := $71;
    DCB.dunit := 1;
    DCB.DCMND := byte('W');
    DCB.DSTATS := _W;
    DCB.DBUFA := word(buf);
    DCB.DTIMLO := $0f;
    DCB.DBYT := len;
    DCB.DAUX1 := Lo(len);    
    DCB.DAUX2 := Hi(len);    
    ExecSIO;    
end;

procedure FN_ReadBuffer(buf: pointer;len: word);
begin
    DCB.DDEVIC := $71;
    DCB.dunit := 1;
    DCB.DCMND := byte('R');
    DCB.DSTATS := _R;
    DCB.DBUFA := word(buf);
    DCB.DTIMLO := $0f;
    DCB.DBYT := len;
    DCB.DAUX1 := Lo(len);    
    DCB.DAUX2 := Hi(len);    
    ExecSIO;    
end;

procedure FN_ReadStatus(status: pointer);
begin
    DCB.DDEVIC := $71;
    DCB.dunit := 1;
    DCB.DCMND := byte('S');
    DCB.DSTATS := _R;
    DCB.DBUFA := word(status);
    DCB.DTIMLO := $0f;
    DCB.DBYT := 4;
    DCB.DAUX1 := 0;    
    DCB.DAUX2 := 0;    
    ExecSIO;    
end;

procedure FN_Close;
begin
    DCB.DDEVIC := $71;
    DCB.dunit := 1;
    DCB.DCMND := byte('C');
    DCB.DSTATS := _NO;
    DCB.DBUFA := 0;
    DCB.DAUX1 := 0;    
    DCB.DAUX2 := 0;   
    ExecSIO;
end;

end.
