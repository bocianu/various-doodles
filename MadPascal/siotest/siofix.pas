program siotest;
uses crt;

var proceedFlag, interruptFlag:byte;
    VPRCED:word absolute $0202;
    VINTER:word absolute $0204;
    PACTL:byte absolute $D302;
    PBCTL:byte absolute $D303;
    oldHandler:word;
    
procedure PFlagHandler:interrupt;assembler;
asm {
    pla
    lda #$01
    sta proceedFlag
};
end;

procedure iFlagHandler:interrupt;assembler;
asm {
    pla
    lda #$01
    sta interruptFlag
};
end;

begin
    PACTL := PACTL or 1;
    PBCTL := PBCTL or 1;
    // set Interrupts
    asm { sei};
    oldHandler := VPRCED;
    VPRCED := word(@PFlagHandler);
    VINTER := word(@iFlagHandler);
    asm { cli};
    
    repeat 
        Pause;
        if interruptFlag = 1 then begin
            Writeln('Slope detected on Interrupt line');
            interruptFlag := 0;
        end;
        if proceedFlag = 1 then begin
            Writeln('Slope detected on Proceed line');
            proceedFlag := 0;
        end;
    until keypressed;

    asm { sei};
    VPRCED := oldHandler;
    VINTER := oldHandler;
    asm { cli};


end.
