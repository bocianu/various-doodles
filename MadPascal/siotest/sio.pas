unit sio;

interface

type DCB_struct = record
    DDEVIC  :byte;
    DUNIT   :byte;
    DCMND   :byte;
    DSTATS  :byte;
    DBUFA   :word;
    DTIMLO  :byte;
    DUNUSE  :byte;
    DBYT    :word;
    DAUX1   :byte;
    DAUX2   :byte;
end;

const 
    _R = $40; 
    _W = $80;
    _RW = $C0;
    _NO = $00;
    JSIOINT = $E459;


var DCB: DCB_struct absolute $300;
    PACTL: byte absolute $D302;
    PBCTL: byte absolute $D303;
    VPRCED:   word absolute $0202;
    VINTER:   word absolute $0204;
    sioResult: byte;

procedure ExecSIO:assembler;

implementation

procedure ExecSIO;assembler;
asm {
        jsr JSIOINT  
        sty sioResult 
    };
end;

end.
